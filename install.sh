#!/usr/bin/env bash

set -e

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
if [[ ! -f "${DIR}/config.sh" ]]; then
  echo "${DIR}/config.sh is not existed." >&2
  exit 1
fi

source "${DIR}/config.sh"

# -b: disable push block dns

docker run -v "${VPN_DATA}":/etc/openvpn -it --rm \
  -e 'OVPN_DISABLE_PUSH_BLOCK_DNS=1' \
  kylemanna/openvpn ovpn_genconfig -u "udp://${VPN_SERVER}" -b

docker run -it -v "${VPN_DATA}":/etc/openvpn --rm kylemanna/openvpn ovpn_initpki nopass

docker-compose up -d
